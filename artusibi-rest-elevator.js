/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var restElevatorType = function(context) {
        var elevator = this;

        elevator.context = context;

        /**
         * Ordered list of the floor numbers conneted by the elevator.
         */
        elevator.floors = [];

        /**
         * Remote kitchens sorted by floor.
         */
        elevator.remoteKitchensByFloor = {};
    };

    restElevatorType.prototype.init = function() {
        var dr = $.Deferred();
        var elevator = this;

        // copy rest elevator specific options into easy typing options.
        $.each(elevator.conf["se:remoteKitchens"], function(index, remoteKitchen) {
            var kitchenFloor = remoteKitchen.floor;
            if (!kitchenFloor) {
                // if not specified, floor of the kitchen is its position in remoteKitchen array
                kitchenFloor = index;
            }

            // register the floor of current kitchen if not yet registered
            if ($.inArray(kitchenFloor, elevator.floors) == -1) {
                elevator.floors.push(kitchenFloor);
            }

            // register kitchen in floor
            if(!elevator.remoteKitchensByFloor[kitchenFloor]) {
                elevator.remoteKitchensByFloor[kitchenFloor] = [];
            }
            elevator.remoteKitchensByFloor[kitchenFloor].push(remoteKitchen);
        });

        // sort floor numbers in ascending numerical order
        elevator.floors.sort(function(a,b){return a - b});

        dr.resolve();

        return dr.promise();
    };

    /**
     * Deliver the message to the kitchens connected by this elevator.
     * Starting from the first floor and up, the elevator stops and come back
     * to the sender kitchen as soon as a response is obtained.
     *
     * @options the message to be delivered with enclosed options.
     **/
    restElevatorType.prototype.deliverMessage = function(options) {
        var elevator = this;

        var jsonMsg = JSON.stringify(options);

        var stopAtFloor = function(floorIndex) {
            var dr = $.Deferred();

            var floor = elevator.floors[floorIndex];

            // deliver message to all kitchen at floor in parallel
            var drs = [];
            $.each(elevator.remoteKitchensByFloor[floor], function(remoteKitchenIndex, remoteKitchen) {
                //var drl = tools.restCall(remoteKitchen["se:kitchenUrl"], {options: jsonMsg});
                var drl = tools.restCall(window.location.href, {ssoupRequestType: 'message', options: jsonMsg});
                drs.push(drl);
            });

            $.when.apply( $, drs ).done(function(results){
                var resultsFound = results;

                if(resultsFound || floorIndex == elevator.floors.length - 1) {
                    // if message successfully delivered to at least one kitchen or last floor reached go back home
                    var ret = [];
                    if (elevator.remoteKitchensByFloor[floor].length == 1) {
                        resultsFound = [resultsFound];
                    }

                    $.each(resultsFound, function(remoteKitchenIndex, resultFoundArray) {
                        if (resultFoundArray) {
                            if (!$.isArray(resultFoundArray)) {
                                console.error("Result MUST be an array: " + resultFoundArray);
                            } else {
                                // properties of arrays cannot be serialized in json, setting @type again after transmission
                                resultFoundArray["@type"] = "sw:resultList";

                                $.each(resultFoundArray, function(index, resultFound) {
                                    if (!resultFound) {
                                        return true; // continue
                                    }

                                    // keep track of provenance
                                    if (resultFound["sw:provenance"] && resultFound["sw:provenance"]["prov:atLocation"]) {
                                            if (!$.isArray(resultFound["sw:provenance"]["prov:atLocation"])) {
                                                resultFound["sw:provenance"]["prov:atLocation"] = [elevator.getIRI(), resultFound["sw:provenance"]["prov:atLocation"]];
                                            } else {
                                                resultFound["sw:provenance"]["prov:atLocation"].splice(0, 0, elevator.getIRI());
                                            }
                                    }

                                    if (options.postProcess == "notEmptyAddOrigin") {
                                        var kitchenUrl = elevator.remoteKitchensByFloor[floor][remoteKitchenIndex]["se:kitchenUrl"];
                                        if (resultFound.origin) {
                                            resultFound["@type"] == "sw:valueWithOrigin";
                                            if (!$.isArray(resultFound.origin)) {
                                                resultFound.origin = [kitchenUrl, resultFound.origin];
                                            } else {
                                                resultFound.origin.splice(0, 0, kitchenUrl);
                                            }
                                        } else if (!$.isArray(resultFound) || resultFound.length > 0) {
                                            if (!$.isArray(resultFound)) {
                                                resultFound = [resultFound];
                                            }
                                            resultFound = {
                                                "@type": "sw:valueWithOrigin",
                                                origin: [kitchenUrl],
                                                value: resultFound
                                            };
                                        }
                                    }
                                });
                                ret = ret.concat(resultFoundArray);
                                ret["@type"] = "sw:resultList";
                            }
                        }
                    } );
                    dr.resolve(ret);
                } else {
                    // else move to next floor
                    stopAtFloor(floorIndex + 1).done(drdf(dr)).fail(drff(dr));
                }
            })
            .fail(drff(dr));

            return dr.promise();
        };

        return stopAtFloor(0).promise();
    };

    return restElevatorType;
});
